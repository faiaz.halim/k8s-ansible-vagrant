## Installation Instructions

1. Download repo from ```develop``` branch

```
git clone -b develop git@gitlab.com:faiaz.halim/k8s-ansible-vagrant.git
```

2. Install Vagrant using

```
make install-vagrant
```

3. Deploy Vagrant vms with ```virtualbox``` provider with command

```
make deploy-vagrant
```

4. Ansible script ```playbook.yml``` is going to run at first startup. 

## Kubernetes Setup

Ansible script will install ```microk8s``` in 1 master and 2 worker nodes. More configurations ongoing.

## Current issues

1. Vagrant won't complete vm setup. Need to troubleshoot ssh timeout and assigning static ip. This is first time vagrant setup so knowledge is limited.

2. Ansible inventory and hosts file are loaded default vagrant ones. Need to update config to load project directory ones.

3. Ansible scripts need to have conditional statements so they can be added to microk8s cluster. Further configuration pending.