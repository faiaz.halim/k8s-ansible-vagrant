include .env

update:
	git pull
	cp .env_ .env

install-vagrant:
	sudo apt install virtualbox
	sudo wget https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb
	sudo dpkg -i vagrant_2.2.9_x86_64.deb

download-vagrant-box:
	vagrant box add hashicorp/bionic64 --force

deploy-vagrant:
	sudo vagrant up

reload-vagrant:
	sudo vagrant reload

status-master:
	vagrant status master

status-worker1:
	vagrant status worker2

status-worker2:
	vagrant status worker2

ssh-master:
	vagrant ssh master

ssh-worker1:
	vagrant ssh worker1

ssh-worker2:
	vagrant ssh worker2

destroy-vagrant:
	sudo vagrant destroy

install-ansible:
	sudo apt install software-properties-common
	sudo add-apt-repository ppa:deadsnakes/ppa
	sudo apt install python3.7
	sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1
	sudo apt install python3-pip
	python3 --version
	sudo -H pip3 install ansible
	ansible --version
